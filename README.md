# XDG Autostart
Start these with whatever, I recommend ```lxsession```.
## Programs
- dunst
- blueman
- bluez
- nm-applet
- pasystray
- pa-notify (aur)
- picom
- nitrogen
- flameshot
- variety
## File locations
```picom.conf``` --> ```~/.config/picom/```(create if doesn't exist) 

```config.rasi``` --> ```~/.config/rofi/```(create if doesn't exist)

all desktop files in root of repo --> ```/etc/xdg/autostart/```

all desktop files in ```home``` directory of repo --> ```~/.config/autostart``` (create if doesn't exist)

files in ```bin``` directory of repo --> ```/usr/bin/```
